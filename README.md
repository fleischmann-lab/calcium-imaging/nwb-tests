# NWB tests

## Setup

```{sh}
pip install -e .
```

## Testing

```{sh}
pytest -v
```
