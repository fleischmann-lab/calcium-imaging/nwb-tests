# Questions/statements to tests

Update, expand/merge as we go.

## TODO

Big question: Can we update an NWB file?

- [ ] Q1: Can we append/modify (scalar or vector) metadata (stored on top level) of an NWB file?
- [ ] Q2: Can we append/modify (scalar or vector) metadata (stored in deeper levels) of an NWB file?
- [ ] Q3: Can we append/modify `TimeSeries` objects, e.g. the timestamps or data of them?
- [ ] Q4: Can we append new columns/rows, delete or modify existing entries of table-like objects?
- [ ] Q5: Can we append/modify containers?
- [ ] Q6: Can we update NWB files (using the above operations) when they are linked (modular storage)? Are the exported files affected?
- [ ] Q7: Can we update data objects with compression option, i.e. an original NWB file being 1 GB, now we want to use compression and chunking to optimize storage?
- [ ] Q8: How might `dandi organize` affect modular storage options when it renames files? Would there be any naming collision when the modalities are similar?

## DONE

- [x] Recursion? Need clarification, addressed by ?
- [x] Copy containers? Need clarification, addressed by ?
