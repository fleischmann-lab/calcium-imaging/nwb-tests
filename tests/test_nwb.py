from pathlib import Path
from pynwb import NWBHDF5IO
from zipfile import ZipFile
from datetime import datetime
from dateutil.tz import tzlocal
from pynwb import NWBFile
from pynwb import TimeSeries
from pynwb import NWBHDF5IO
import numpy as np
from pynwb import get_manager


DATA_PATH = Path("data")


def test_create_new_object_then_export():
    # https://github.com/NeurodataWithoutBorders/pynwb/issues/1301

    # File from Suite2p
    nwb_path_zip = DATA_PATH / "ophys.zip"
    with ZipFile(nwb_path_zip, "r") as zip_ref:
        zip_ref.extractall(DATA_PATH)
    nwb_path = DATA_PATH / "ophys.nwb"

    with NWBHDF5IO(str(nwb_path), "r") as read_io:
        nwbfile = read_io.read()
        img_seg = nwbfile.processing.get("ophys").data_interfaces.get(
            "ImageSegmentation"
        )
        PlaneSegmentationOld = img_seg.get_plane_segmentation("PlaneSegmentation")

        # Remove the old container and recreate a new one
        img_seg.plane_segmentations.pop("PlaneSegmentation")
        PlaneSegmentationNew = img_seg.create_plane_segmentation(
            description=PlaneSegmentationOld.description,
            imaging_plane=PlaneSegmentationOld.imaging_plane,
            name=PlaneSegmentationOld.name,
            reference_images=PlaneSegmentationOld.reference_images,
        )

        # Dummy data
        pix_mask = [[(0, 0, 1.1), (1, 1, 1.2), (2, 2, 1.3)], [(0, 0, 2.1), (1, 1, 2.2)]]
        iscell = [0, 1]

        # This is what I'm trying to replace in the old file but it crashes without this code block
        for elem in pix_mask:
            PlaneSegmentationNew.add_roi(pixel_mask=elem)
        # PlaneSegmentationOld["iscell"][()] = iscell  # <-- Doesn't work
        PlaneSegmentationNew.add_column(
            "iscell", "two columns - iscell & probcell", iscell
        )

        # Export the file to a new location
        export_filename = nwb_path.parent.joinpath("tmp.nwb")
        with NWBHDF5IO(str(export_filename), mode="w") as export_io:
            export_io.export(src_io=read_io, nwbfile=nwbfile)

    # Remove the old file and replace it with the new one
    nwb_path.unlink()
    export_filename.rename(nwb_path)

    # Try to read the file but it seems corrupted
    with NWBHDF5IO(str(nwb_path), "r") as read_io:
        nwbfile = read_io.read()  # <-- This is where it should crash
        print(nwbfile)
        assert nwbfile
        np.testing.assert_array_equal(
            nwbfile.processing["ophys"]["ImageSegmentation"][
                "PlaneSegmentation"
            ].iscell.data[:],
            [0, 1],
        )

    nwb_path.unlink()


def test_modify_data_then_export():
    # File from Suite2p
    nwb_path_zip = DATA_PATH / "ophys.zip"
    with ZipFile(nwb_path_zip, "r") as zip_ref:
        zip_ref.extractall(DATA_PATH)
    nwb_path = DATA_PATH / "ophys.nwb"

    wrong_dim_objs = ["Deconvolved", "Fluorescence", "Neuropil"]

    with NWBHDF5IO(str(nwb_path), "a") as read_io:
        nwbfile = read_io.read()

        # These objects need to be transposed
        for k in wrong_dim_objs:
            data_k = np.transpose(nwbfile.processing["ophys"][k][k].data[:])
            # WILL FAIL HERE, this is an HDF5 thing
            # Can't assign to data directly
            # probably need to recreate the full object using the PyNWB API
            nwbfile.processing["ophys"][k][k].data = data_k

        # Export the file to a new location
        export_filename = nwb_path.parent.joinpath("tmp.nwb")
        with NWBHDF5IO(str(export_filename), mode="w") as export_io:
            export_io.export(src_io=read_io, nwbfile=nwbfile)

    # Remove the old file and replace it with the new one
    nwb_path.unlink()
    export_filename.rename(nwb_path)

    # Try to read the file but it seems corrupted
    with NWBHDF5IO(str(nwb_path), "r") as read_io:
        nwbfile = read_io.read()  # <-- This is where it should crash
        print(nwbfile)
        assert nwbfile
        for k in wrong_dim_objs:
            assert nwbfile.processing["ophys"][k][k].data.shape[0] == 2000

    nwb_path.unlink()


# TODO: test the same thing as before but recreating the object from scratch
# instead of assigning


def test_modify_top_level_metadata():
    # File from Suite2p
    nwb_path_zip = DATA_PATH / "ophys.zip"
    with ZipFile(nwb_path_zip, "r") as zip_ref:
        zip_ref.extractall(DATA_PATH)
    nwb_path = DATA_PATH / "ophys.nwb"

    with NWBHDF5IO(str(nwb_path), "a") as read_io:
        nwbfile = read_io.read()

        # Top level metadata <- Fail here
        # This may be an HDF5 thing
        # Can't assign to field directly
        nwbfile.session_description = "UPDATED"

        # Export the file to a new location
        export_filename = nwb_path.parent.joinpath("tmp.nwb")
        with NWBHDF5IO(str(export_filename), mode="w") as export_io:
            export_io.export(src_io=read_io, nwbfile=nwbfile)

    # Remove the old file and replace it with the new one
    nwb_path.unlink()
    export_filename.rename(nwb_path)

    # Try to read the file but it seems corrupted
    with NWBHDF5IO(str(nwb_path), "r") as read_io:
        nwbfile = read_io.read()
        nwbfile.session_description == "UPDATED"
    nwb_path.unlink()


def test_modify_metadata_new_file():
    """
    Read existing file with its metadata,
    modify some metadata, and export to new file
    """

    # Create the base data
    start_time = datetime(2017, 4, 3, 11, tzinfo=tzlocal())
    create_date = datetime(2017, 4, 15, 12, tzinfo=tzlocal())
    data = np.arange(1000).reshape((100, 10))
    timestamps = np.arange(100.0)
    filename = "file.nwb"
    filename_new = "file_new.nwb"
    filepath = DATA_PATH / filename
    filepath_new = DATA_PATH / filename_new

    # Create the demo file
    nwbfile = NWBFile(
        session_description="demonstrate external files",
        identifier="NWBE",
        session_start_time=start_time,
        file_create_date=create_date,
    )
    test_ts = TimeSeries(
        name="test_timeseries", data=data, unit="SIunit", timestamps=timestamps
    )
    nwbfile.add_acquisition(test_ts)
    with NWBHDF5IO(filepath, "w") as io:
        io.write(nwbfile)

    # Read + recreate new file
    with NWBHDF5IO(filepath, "r") as io1:
        nwb_read = io1.read()
        nwbfile_new = NWBFile(
            session_description=nwb_read.session_description + "UPDATED",
            identifier=nwb_read.identifier,
            session_start_time=nwb_read.session_start_time,
            file_create_date=nwb_read.file_create_date,
        )
        timeseries = nwb_read.get_acquisition("test_timeseries")
        nwbfile_new.add_acquisition(timeseries)
        with NWBHDF5IO(filepath_new, "w") as io2:
            # Doesn't work for some reason...
            io2.write(nwbfile_new, link_data=False)

    # Test
    with NWBHDF5IO(filepath_new, "r") as io3:
        nwb_test = io3.read()
        assert "UPDATED" in nwb_test.session_description

    # Teardown
    filepath.unlink()
    filepath_new.unlink()


def test_copy_datasets():
    # https://github.com/NeurodataWithoutBorders/pynwb/issues/1297

    # Create the base data
    manager = get_manager()
    start_time = datetime(2017, 4, 3, 11, tzinfo=tzlocal())
    create_date = datetime(2017, 4, 15, 12, tzinfo=tzlocal())
    data = np.arange(1000).reshape((100, 10))
    timestamps = np.arange(100.0)
    filename1 = "file1.nwb"
    filename2 = "file2.nwb"
    filepath1 = DATA_PATH / filename1
    filepath2 = DATA_PATH / filename2

    # Create the 1st file
    nwbfile1 = NWBFile(
        session_description="demonstrate external files",
        identifier="NWBE1",
        session_start_time=start_time,
        file_create_date=create_date,
    )
    test_ts1 = TimeSeries(
        name="test_timeseries1", data=data, unit="SIunit", timestamps=timestamps
    )
    nwbfile1.add_acquisition(test_ts1)
    with NWBHDF5IO(filepath1, "w") as io:
        io.write(nwbfile1)

    # Check what's inside the file
    with NWBHDF5IO(filepath1, "r") as io:
        f1 = io.read()
        print("\n1st file:")
        print(f1)

    # Create the 2nd file
    nwbfile2 = NWBFile(
        session_description="demonstrate external files",
        identifier="NWBE2",
        session_start_time=start_time,
        file_create_date=create_date,
    )
    test_ts2 = TimeSeries(
        name="test_timeseries2", data=data, unit="SIunit", timestamps=timestamps
    )
    nwbfile2.add_acquisition(test_ts2)
    with NWBHDF5IO(filepath2, "w") as io:
        io.write(nwbfile2)

    # Check what's inside the file
    with NWBHDF5IO(filepath2, "r") as io:
        f2 = io.read()
        print("2nd file:")
        print(f2)

    # Get the first container
    with NWBHDF5IO(filepath1, "r", manager=manager) as io1:
        nwbfile1 = io1.read()
        timeseries_1 = nwbfile1.get_acquisition("test_timeseries1")

        # Add it to the 2nd file
        with NWBHDF5IO(filepath2, "r+", manager=manager) as io2:
            nwbfile2 = io2.read()
            nwbfile2.add_acquisition(timeseries_1)
            print("What I should see in the 2nd file:")
            print(nwbfile2)
            io2.write(nwbfile2, link_data=False)

    # Check what's inside the 2nd file
    with NWBHDF5IO(filepath2, "r") as io:
        f2 = io.read()
        print("What really is in the 2nd file:")
        print(f2)
        ts_copied = f2.acquisition["test_timeseries1"]
        assert ts_copied
        np.testing.assert_array_equal(ts_copied.data[:], data)
        np.testing.assert_array_equal(ts_copied.timestamps[:], timestamps)

    filepath1.unlink()
    filepath2.unlink()


def test_appending():
    # Create the base data
    start_time = datetime(2017, 4, 3, 11, tzinfo=tzlocal())
    create_date = datetime(2017, 4, 15, 12, tzinfo=tzlocal())
    data = np.arange(1000).reshape((100, 10))
    timestamps = np.arange(100.0)
    filename = "file.nwb"
    filepath = DATA_PATH / filename

    # Create the demo file
    nwbfile = NWBFile(
        session_description="demonstrate external files",
        identifier="NWBE",
        session_start_time=start_time,
        file_create_date=create_date,
    )
    test_ts = TimeSeries(
        name="test_timeseries", data=data, unit="SIunit", timestamps=timestamps
    )
    nwbfile.add_acquisition(test_ts)
    with NWBHDF5IO(filepath, "w") as io:
        io.write(nwbfile)

    # Append to the demo file
    # although "append" here means creating a new `TimeSeries`
    data2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    timestamps2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    with NWBHDF5IO(filepath, "a") as io:
        fid = io.read()
        new_time_series = TimeSeries(
            name="new_time_series",
            data=data2,
            timestamps=timestamps2,
            unit="n.a.",
        )
        fid.add_acquisition(new_time_series)
        io.write(fid)
        io.close()

    with NWBHDF5IO(filepath, "r") as io:
        file_io = io.read()
        ts_appended = file_io.acquisition["new_time_series"]
        np.testing.assert_array_equal(ts_appended.data[:], data2)
        np.testing.assert_array_equal(ts_appended.timestamps[:], timestamps2)

    filepath.unlink()


# TODO: Check if possible to copy containers, not datasets

# TODO: If you have 10 timeseries and you want to modify only 1,
# do you need to manually copy all of them, or can you just copy the whole NWB file,
# modify the timeseries, and export everything?
